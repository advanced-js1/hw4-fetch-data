const btn = document.querySelector('.btn');
const movies = document.querySelector('.movies');

function getMovies() {

    fetch('https://ajax.test-danit.com/api/swapi/films')
      	.then(response => response.json())
		.then(films => {
			// console.log(films);
			films.forEach(film => {
				let filmLi = document.createElement('li');
				filmLi.classList.add('film');
				filmLi.classList.add('loading');
				movies.append(filmLi);
				// li.textContent = 'Episode ' + film.episodeId + ' - ' + film.name + ' - ' + film.openingCrawl;
				filmLi.innerHTML = `
					<h3>Episode ${film.episodeId}: ${film.name}</h3>
					<p>${film.openingCrawl}</p>
					<div class="characters-container"></div>
				`;

				film.characters.forEach(characterUrl => {

					fetch(characterUrl)
						.then(response => response.json())
						.then(character => {
							let characterEl = document.createElement('p');
							characterEl.textContent = character.name;
							let characters = filmLi.querySelector('.characters-container');
							characters.append(characterEl);
							filmLi.classList.remove('loading');
					});
				});
			})			
		});  
}

btn.addEventListener('click', () => {
	getMovies();
})